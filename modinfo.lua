name = "firth"
description = "Salad Fingers Character Mod"
author = "HexGear Studio"
version = "0.0.1"

forumthread = ""

api_version = 6

dont_starve_compatible = true

reign_of_giants_compatible = true

shipwrecked_compatible = true

icon_atlas = "modicon.xml"
icon = "modicon.tex"
