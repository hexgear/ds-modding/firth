PrefabFiles = {
    "firth",
}

Assets = {
    Asset( "IMAGE", "images/saveslot_portraits/firth.tex" ),
    Asset( "ATLAS", "images/saveslot_portraits/firth.xml" ),

    Asset( "IMAGE", "images/selectscreen_portraits/firth.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/firth.xml" ),
    
    Asset( "IMAGE", "images/selectscreen_portraits/firth_silho.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/firth_silho.xml" ),

    Asset( "IMAGE", "bigportraits/firth.tex" ),
    Asset( "ATLAS", "bigportraits/firth.xml" ),
    
    Asset( "IMAGE", "images/map_icons/firth.tex" ),
    Asset( "ATLAS", "images/map_icons/firth.xml" ),

}

local require = GLOBAL.require

GLOBAL.STRINGS.CHARACTER_TITLES.firth = "Beware the Friendly Stranger"
GLOBAL.STRINGS.CHARACTER_NAMES.firth = "Salad Fingers"
GLOBAL.STRINGS.CHARACTER_DESCRIPTIONS.firth = ""
GLOBAL.STRINGS.CHARACTER_QUOTES.firth = "\"I like Rusty Spoons\""

GLOBAL.STRINGS.CHARACTERS.FIRTH = require "speech_firth"

table.insert(GLOBAL.CHARACTER_GENDERS.MALE, "firth")

AddMinimapAtlas("images/map_icons/firth.xml")
AddModCharacter("firth")

